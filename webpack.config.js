/*  */
var path = require("path");
var webpack = require('webpack');

module.exports = {
    entry: "./src/goatstone/index.js",
    output: {
        path: path.resolve(__dirname, "public"),
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {test: path.join(__dirname, 'goatstone'), loader: 'babel-loader'}
        ]
    },
    plugins: [
        // Avoid publishing files when compilation failed
        new webpack.NoErrorsPlugin()
    ],
    stats: {
        // Nice colored output
        colors: true
    },
    // Create Sourcemaps for the bundle
    devtool: 'source-map'
};